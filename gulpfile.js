const gulp = require('gulp');
const del = require('del');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const tscConfig = require('./tsconfig.json');
const sass = require('gulp-sass');

// clean the contents of the distribution directory
gulp.task('clean', function () {
  return del('./dist/**/*');
});

gulp.task('copy:index', function() {
  return gulp.src(['index.html','systemjs.config.js'], { base : './' })
    .pipe(gulp.dest('./dist'))
    .pipe(browserSync.stream());
});

gulp.task('copy:templates', function () {
  return gulp.src('./app/templates/*.html')
    .pipe(gulp.dest('./dist/app/templates'))
    .pipe(browserSync.stream());
});

gulp.task('copy:data', function () {
  return gulp.src('./app/data/*.json')
    .pipe(gulp.dest('./dist/app/data'))
    .pipe(browserSync.stream());
});

gulp.task('copy:libs', function() {
	return gulp.src([
		'node_modules/core-js/client/shim.min.js',
		'node_modules/zone.js/dist/zone.js',
		'node_modules/reflect-metadata/Reflect.js',
		'node_modules/systemjs/dist/system.src.js'
	],{ base : './' })
	.pipe(gulp.dest('./dist'))
});

gulp.task('copy:modules', function() {
	return gulp.src([
		'node_modules/@angular/common/**/*',
		'node_modules/@angular/compiler/**/*',
		'node_modules/@angular/core/**/*',
		'node_modules/@angular/http/**/*',
		'node_modules/@angular/platform-browser/**/*',
		'node_modules/@angular/platform-browser-dynamic/**/*',
		'node_modules/@angular/router/**/*',
		'node_modules/@angular/router-deprecated/**/*',
		'node_modules/@angular/upgrade/**/*',
		'node_modules/angular2-in-memory-web-api/**/*',
		'node_modules/rxjs/**/*'
	],{ base : './' })
	.pipe(gulp.dest('./dist'))
});

gulp.task('copy:styles', function () {
  console.log('styles changed')
  return gulp.src('./app/styles/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/app/styles'));
});
gulp.task('watch-copy:styles', ['copy:styles'], browserSync.reload);



gulp.task('copy', ['copy:index', 'copy:libs', 'copy:data', 'copy:templates', 'copy:styles', 'copy:modules']);

gulp.task('compile', function () {  
  var tsResult = gulp.src('./app/**/*.ts')
    .pipe(sourcemaps.init())
    .pipe(ts(tscConfig.compilerOptions));
  return tsResult.js
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest('./dist/app'))
    .pipe(browserSync.stream());
});



gulp.task('webserver', ['copy', 'compile'], function() {
  
  browserSync.init({
    server: "./dist",
    port: 3000,
    logFileChanges:false
  });
  
  gulp.watch('./app/**/*.ts', ['compile']);
  gulp.watch('./app/templates/*.html', ['copy:templates']);
  gulp.watch('./app/data/*.json', ['copy:data']);
  gulp.watch('./index.html', ['copy:index']);
  gulp.watch('./app/styles/*.scss', ['watch-copy:styles']);

});

gulp.task('default', ['webserver']);

