# Brian's Angular2 Starter

## Description

I use this simple app as a starter app for my new Angular 2 projects. It took me a while to create a '/app' folder structure that I feel comfortable working with.

The app definitely does not do anything special. Some basic routing and some injectable services.

I will continuously reshape this to become a better starter project for my future Angular2 projects.

The next step is adding a form sample. Maybe next weekend.

## Installation

```
npm install
```

## Starting the application

```
gulp
```