import {Component} from '@angular/core';
import { Routes, ROUTER_DIRECTIVES } from '@angular/router';
import { AboutHomeComponent} from './aboutHome.component';
import { AboutWhatComponent} from './aboutWhat.component';
@Component({
	selector: 'about-page',
	templateUrl: './app/templates/about.component.html',
	directives:[ROUTER_DIRECTIVES]
})
@Routes([
		{ path: '/', component: AboutHomeComponent },
		{ path: '/:what', component: AboutWhatComponent }
])
export class AboutComponent{
	abouts = ['history', 'science', 'technology', 'art'];
}