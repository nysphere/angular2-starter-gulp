import { Component } from '@angular/core';
import { RouteSegment } from '@angular/router';

@Component({
	selector: 'about-history-page',
	templateUrl: './app/templates/aboutWhat.component.html'
})
export class AboutWhatComponent {
	what;
	constructor(routeSegment: RouteSegment) {
		this.what = routeSegment.getParam('what');
	}
}