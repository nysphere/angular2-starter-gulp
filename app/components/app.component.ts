import { Component, OnInit } from '@angular/core';
import { Routes, ROUTER_DIRECTIVES } from '@angular/router';

import { AppService } from '../services/app.service'; 
import { ExampleService } from '../services/example.service'; 
import { HomeComponent } from './home.component';
import { AboutComponent } from './about.component';
import { ContactComponent } from './contact.component';


@Component({
	selector:'app',
	templateUrl:`./app/templates/app.component.html`,
	styleUrls: [`./app/styles/app.component.css`],
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: '/', component: HomeComponent},
	{ path: '/about', component: AboutComponent},
	{ path: '/contact', component: ContactComponent}
])
export class AppComponent implements OnInit{
	greeting;
	test;
	constructor(private appService: AppService, private exampleService: ExampleService) {}

	ngOnInit(){
		
		this.greet(this)

		this.exampleService.fetch().subscribe(data => {
			this.test = JSON.stringify(data);
		});

	}

	greet(_this) {
		_this.greeting = _this.appService.next();
		setTimeout(_this.greet, 3000, _this);
	}
	


}