import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class ExampleService{
	
	constructor(private http:Http){

	}

	fetch(){
		return this.http.get('./app/data/example.json').map(res => res.json())
	}
}