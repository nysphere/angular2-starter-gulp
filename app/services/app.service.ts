import { Injectable, OnInit } from '@angular/core';
import 'rxjs/Rx';


@Injectable()
export class AppService implements OnInit{
	private greetings = ['Hello World', 'Hello Universe', 'Hello Angular'];
	private sharedData = 'Hello';
	i=0;
	constructor(){
		
	}

	next(){
		this.sharedData = this.greetings[this.i];
		this.i += 1;
		this.i = this.i % this.greetings.length
		return this.sharedData;
	}

	ngOnInit(){

	}
}