///<reference path="../typings/globals/es6-shim/index.d.ts"/>
import { bootstrap } from '@angular/platform-browser-dynamic';
import { AppComponent } from './components/app.component';
import { HTTP_PROVIDERS } from '@angular/http'
import { AppService } from './services/app.service'; 
import { ExampleService } from './services/example.service'; 
import { ROUTER_PROVIDERS } from '@angular/router';

//import { provide } from '@angular/core';
//import { LocationStrategy, PathLocationStrategy } from '@angular/common'; // or HashLocationStrategy


bootstrap(AppComponent, [
	HTTP_PROVIDERS, 
	ROUTER_PROVIDERS, 
	AppService, 
	ExampleService//,
	// provide(LocationStrategy, {useClass: PathLocationStrategy})
]);